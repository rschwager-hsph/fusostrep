import re
import sys
import contextlib

base_name = lambda s: re.sub(r'\..*', '\1', s)

def total_rpkm(f):
    rpkms = iter(float(l.strip().split()[-1]) for l in f)
    return sum(rpkms)


def main(base_rpkm, *other_rpkms):
    other_fs = map(open, other_rpkms)
    with open(base_rpkm) as base, contextlib.nested(*other_fs):
        msg = "{base}:{comp}\t{ratio}"
        total_base = total_rpkm(base)
        print msg.format(base=base_name(base_rpkm), 
                         comp=base_name(base_rpkm), 
                         ratio=total_base/total_base)
        for to_compare in other_fs:
            total_compare = total_rpkm(to_compare)
            print msg.format(base=base_name(base_rpkm),
                             comp=base_name(to_compare.name),
                             ratio=total_compare/total_base)


if __name__ == '__main__':
    ret = main(*sys.argv[1:])
    sys.exit(ret)
