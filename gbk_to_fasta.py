import re
import sys

from Bio import SeqIO

def load_mapping(mapping_fname):
    the_mapping = {}
    with open(mapping_fname) as f:
        key = ""
        for line in f:
            if re.match(r'^\s+\d', line):
                the_mapping[key] = line.strip().split(None, 1)[1]
            else:
                key = line.strip()
    return the_mapping


def main(genbank_fname, mapping_fname):
    mapping = load_mapping(mapping_fname)
    seqs = SeqIO.parse(genbank_fname, 'genbank')
    for seq in seqs:
        for feature in seq.features:
            gene_name = feature.qualifiers.get("gene", [""])[0]
            if gene_name in mapping:
                kbase_id = mapping[gene_name]
                seq = feature.qualifiers.get('translation', [''])[0]
                if seq:
                    print ">{} {}\n{}".format(gene_name, kbase_id, seq)


if __name__ == '__main__':
    ret = main(*sys.argv[1:])
    sys.exit(ret)
