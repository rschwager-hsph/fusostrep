#!/usr/bin/env python

import sys
from itertools import count
from operator import attrgetter as geta
from collections import namedtuple

from Bio import SeqIO
from toolz import groupby, concatv, compose
from toolz.curried import get

headers = ["Location", "Strand", "Length", "PID",
           "Gene", "Synonym Code", "COG", "Product"]
pidcounter = count(1)

startloc = lambda f: f.location.start

def sequences(gbk_file):
    return SeqIO.parse(gbk_file, 'genbank')

def format_feature(feature):
    return "\t".join( [
        "{}..{}".format(feature.location.start, feature.location.end), #Location
        ["?", "+", "-"][feature.location.strand], #Strand
        str(len(feature)), # Length
        str(next(pidcounter)), # PID
        feature.qualifiers.get("gene", ["-"])[0], # Gene
        feature.qualifiers['locus_tag'][0], # Synonym Code
        "-", # COG
        ",".join(feature.qualifiers.get("product",["-"])) # Product
    ] )+"\n"

def main(gbk_file, ptt_file, rnt_file):
    with open(ptt_file, 'w') as ptt_f, open(rnt_file, 'w') as rnt_f:
        for seq in sequences(gbk_file):
            msg = "{id} - 1..{l}\n{nfeat} {featclass}\n{headers}\n"
            grp = groupby(geta("type"), seq.features)
            prot_features = sorted(grp['CDS'], key=startloc)
            rna_features = concatv(grp.get("rRNA", []),
                                   grp.get("tRNA", []),
                                   grp.get("tmRNA", []))
            rna_features = sorted(rna_features, key=startloc)
            ptt_f.write(msg.format(id=seq.id, l=len(seq),
                                   nfeat=len(prot_features),
                                   featclass="proteins",
                                   headers="\t".join(headers)))
            rnt_f.write(msg.format(id=seq.id, l=len(seq),
                                   nfeat=len(rna_features),
                                   featclass="RNAs",
                                   headers="\t".join(headers)))
            map(compose(ptt_f.write, format_feature), prot_features)
            map(compose(rnt_f.write, format_feature), rna_features)
            
    

if __name__ == '__main__':
    ret = main(*sys.argv[1:])
    sys.exit(ret)
