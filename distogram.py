import re
import sys
from itertools import izip, count, combinations

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm


def l2norm(a, b):
    return np.linalg.norm(a-b, ord=2)

def l2logfold(a, b):
    avail = a.astype(bool) & b.astype(bool)
    folds = np.log( a[avail]/b[avail] )
    return np.linalg.norm(folds, ord=2)

def distance_matrix(distance_func, points):
    n = len(points)
    ret = np.zeros((n,n))
    for (a, i), (b, j) in combinations(izip(points, count()), 2):
        ret[i,j] = distance_func(a, b)
    return np.ma.array(ret, mask=np.tri(n, k=-1))

def plot(out_fname, distance_mtx, title, names):
    figure = plt.figure()
    axis = figure.add_subplot(111)
    cmap = cm.get_cmap("Greens", 500)
    cmap.set_bad('w')
    plt.xticks(np.arange(len(names)), names)
    plt.yticks(np.arange(len(names)), names)
    plt.title(title)
    img = axis.imshow(distance_mtx, interpolation="nearest", cmap=cmap)
    plt.colorbar(img)
    plt.savefig(out_fname)

def main(out_fname, *in_lists):
    points = [ np.loadtxt(f, dtype=float) for f in in_lists ]
    distances = distance_matrix( l2logfold, points )
    title = re.compile(r'(\S+)T(\d+.?)\..*$').match(in_lists[0]).group(1)
    names = [ re.compile(r'.*T(\d+.?)\..*').match(s).group(1)
              for s in in_lists ]
    plot(out_fname, distances, title, names)

if __name__ == "__main__":
    ret = main(*sys.argv[1:])
    sys.exit(ret)
