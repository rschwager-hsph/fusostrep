import sys
from itertools import tee, imap, ifilter
from toolz import pluck, compose
from toolz.curried import get

fields = lambda str: iter( line.strip().split('\t') for line in open(str) )
get_fpkm = compose(float, get(1))
fpkms = fields(sys.argv[1])

mapping = list(fields('filtered_joined_mapping'))
strep_set = set(pluck(1, mapping))
strep_filter = lambda fpkm: ">"+fpkm[0].replace("_PROKKA","") in strep_set
fuso_set = set(pluck(0, mapping))
fuso_filter = lambda fpkm: ">"+fpkm[0].replace("_PROKKA","") in fuso_set

fuso_fpkms, strep_fpkms = tee(fpkms)
fuso_fpkms = ifilter( fuso_filter, fuso_fpkms)
strep_fpkms = ifilter( strep_filter, strep_fpkms)
fuso_total = sum( imap(get_fpkm, fuso_fpkms) )
strep_total = sum( imap(get_fpkm, strep_fpkms) )

print "fuso_total: %s\tstrep_total: %s"%(fuso_total, strep_total)
