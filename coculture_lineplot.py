import sys
import contextlib
from glob import glob
from itertools import count

import numpy as np
import matplotlib.pyplot as plt
from toolz import get, first, second

figcount = count(1)

def fields(f):
    flds = iter( get([3, 6], l.strip().split('\t')) for l in f
                 if not l.startswith("Start") )
    return sorted(filter(None, flds), key=first)


def load_expression_array(name_base):
    files = map(open, glob(name_base+"*.txt"))
    with contextlib.nested(*files):
        deserialized = map(fields, files)
        first, rest = deserialized[0], deserialized[1:]
        ids, _ = zip(*first)
        expr_array = np.array([map(second, item) for item in deserialized],
                              dtype=float)
    return ids, expr_array
            

def load(*name_bases):
    arrays = map(load_expression_array, name_bases)
    ids = arrays[0][0]
    return ids, map(second, arrays)


def distance_l2(a_arr, b_arr):
    diffs = a_arr-b_arr
    return np.linalg.norm(diffs, ord=2, axis=1)


def distance_logfold(a_arr, b_arr):
    ret = []
    for i in range(a_arr.shape[0]):
        a, b = a_arr[i], b_arr[i]
        avail = a.astype(bool) & b.astype(bool)
        folds = np.log( a[avail]/b[avail] )
        ret.append(np.linalg.norm(folds, ord=2))
    return ret


def plot(a, a_label, output):
    plt.figure(figcount.next())
    plt.plot(a, 'r', label=a_label)
    plt.plot(a, 'ro')
    plt.xticks(np.arange(len(a)), ["0", "30m", "1h", "2h", "4h"])
    plt.grid(True)
    plt.legend()
    plt.savefig(output)


def main(coculture_basestr, fusomono_basestr, strepmono_basestr,
         fuso_out_fname, strep_out_fname):
    ids, (co_expr, fuso_expr, strep_expr) = load(coculture_basestr,
                                                 fusomono_basestr,
                                                 strepmono_basestr)
    co_fuso_distances = distance_l2(co_expr, fuso_expr)
    co_strep_distances = distance_l2(co_expr, strep_expr)
    # drop the 24h (last) distance point because the coculture didn't
    # yield any data at 24h
    plot(co_fuso_distances[:-1], "Fuso Distance", fuso_out_fname)
    plot(co_strep_distances[:-1],"Strep Distance",strep_out_fname) 
    

if __name__ == '__main__':
    ret = main(*sys.argv[1:])
    sys.exit(ret)
