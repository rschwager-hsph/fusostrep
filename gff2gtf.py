#!/usr/bin/env python

import re
import sys

def _fields(name):
    with open(name) as f:
        for line in f:
            line = line.strip()
            fields = []
            if line.startswith("##"):
                continue

            if line.startswith(">"):
                break

            yield line.split('\t')



def _output(fields, attributes, i):
    mandatory_attrs = [("gene_id", attributes['ID']), 
                       ("transcript_id", attributes['ID'])]
    attr_field = " ".join('%s "%s";'%s for s in mandatory_attrs + attributes.items())
    fields[-1] = attr_field
    if fields[2] == "CDS":
        fields[2] = "exon"
    print '\t'.join(fields)


def main(gff_filename):
    fields = _fields(gff_filename)
    for i, field_line in enumerate(fields):
        attributes = dict(re.findall(r'(.+?)=(.+?);', field_line[-1]))
        _output(field_line, attributes, i)


if __name__ == '__main__':
    ret = main(sys.argv[1])
    sys.exit(ret)
