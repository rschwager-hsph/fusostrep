import re
import sys
import string
import operator
import contextlib
from itertools import ifilter, chain, tee

from toolz import mapcat, join, pluck, get, compose, curry, first

gt = curry(operator.lt)

fields = lambda f: iter( l.strip().split('\t') for l in f )
fields_n = lambda f: iter( l.strip().split() for l in f )
w = lambda fields: sys.stdout.write('\t'.join(fields)+'\n')
prot_start = lambda v: v[0].split("..")[0]
headers = "Start Stop Strand ID Gene Count Rpkm Product".split()

def main(prot_table, *rpkm_files):
    rpkm_fs = map(open, rpkm_files)
    with open(prot_table) as prot_f, contextlib.nested(*rpkm_fs):
        rpkm_fields = ifilter( compose(gt(4), len), mapcat(fields_n, rpkm_fs) )
        prot_fields = ifilter( compose(gt(4), len), fields(prot_f) )
        prot_a, prot_b = tee(prot_fields)
        rpkm_a, rpkm_b = tee(rpkm_fields)
        joined_a = join(prot_start, prot_a, 0, rpkm_a)
        joined_b = join(5, prot_b, 0, rpkm_b)
        print "\t".join(headers)
        for prot_l, rpkm_l in chain(joined_a, joined_b):
            a = get([1, 4, 5, 7], prot_l)
            b = get([1, 2, 4, 5], rpkm_l)
            to_print = "\t".join( get([4, 5, 0, 2, 1, 6, 7, 3], a+b) )
            if "Start" in to_print:
                pass
            else:
                print to_print
    

if __name__ == "__main__":
    ret = main(*sys.argv[1:])
    sys.exit(ret)
