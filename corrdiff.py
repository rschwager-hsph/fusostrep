import os
import re
import sys
import operator
import contextlib
from glob import glob
from itertools import count, starmap, combinations, imap, tee, izip

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
from toolz import get, first, second, curry, cons, last

gt = curry(operator.lt)
gte = curry(operator.le)

def running_average(it):
    sm, cnt = float(), float()
    sum_copy, count_copy = tee(it)
    for to_sum, to_count in izip(sum_copy, count_copy):
        sm += to_sum
        cnt += 1
        yield sm/cnt

    
def fields(f):
    flds = iter( get([3, 6], l.strip().split('\t')) for l in f
                 if not l.startswith("Start") )
    return sorted(filter(None, flds), key=first)


def load_expression_array(name_base):
    files = map(open, glob(name_base+"*.txt"))
    with contextlib.nested(*files):
        deserialized = map(fields, files)
        first, rest = deserialized[0], deserialized[1:]
        ids, _ = zip(*first)
        expr_array = np.array([map(second, item) for item in deserialized],
                              dtype=float)
        t = times(name_base)
        sort_idxs = np.argsort(t)
    return ids, (get(list(sort_idxs), t), expr_array[sort_idxs,:])
            

def load(*name_bases):
    arrays = map(load_expression_array, name_bases)
    ids = arrays[0][0]
    return ids, map(second, arrays)

time_re = lambda s : re.search(r'(\d+)([mh]?)', s).groups()

def times(basestr):
    filenames = glob(basestr+"*.txt")
    units_m = {"m": 1, "h": 60, "": 0}
    vals, units = zip(* map(time_re, filenames))
    vals, units = map(units_m.get, units), map(int, vals)
    return map(operator.mul, vals, units)

def pearsons_corrs(expr_tab, times):
    corr_against_time = lambda row: np.corrcoef(row, times)[0,1]
    return map(corr_against_time, expr_tab.T)

def is_sparse(expr_tab, min_abd):
    """find genes with expression sums greater than some proportion
    (min_abd) of the average total abundance for a time point

    """
    return expr_tab.sum(axis=0) > (min_abd*expr_tab.sum(axis=1).mean())
    

def corr_skipnone(pairs):
    pairs = [pair for pair in pairs if pair[0] is not None]
    return np.corrcoef(*zip(*pairs))[0,1]

def correlation_permutation(mono_t, mono_expr, co_t, co_expr):
    mono_pairs, co_pairs = zip(mono_t, mono_expr), zip(co_t, co_expr)
    lendiff = len(mono_t) - len(co_t)
    if lendiff > 0:
        co_pairs.extend([(None, None)]*lendiff)
        larger = len(mono_t)
    else:
        mono_pairs.extend([(None, None)]*(-lendiff))
        larger = len(co_t)
    trues_falses = [(True, True)]*larger + [(False, False)]*larger
    choice_combos = combinations(trues_falses, larger)
    def _diffs():
        for choices in choice_combos:
            a = np.where(choices, mono_pairs, co_pairs)
            b = np.where(choices, co_pairs, mono_pairs)
            yield (corr_skipnone(a) - corr_skipnone(b))**2
    diffs = _diffs()
    original = next(diffs)
    more_extreme = imap(gte(original), cons(original, diffs))
    p_value = last(running_average(more_extreme))
    return p_value


def correlation_permutations(mono_t, mono_expr, co_t, co_expr):
    packs = iter( (mono_t, mono_e, co_t, co_e)
                  for mono_e, co_e in zip(mono_expr.T, co_expr.T) )
    return list(starmap(correlation_permutation, packs))

cmap = plt.get_cmap("RdYlGn")
color_space = np.linspace(0,1,num=100)
colors = cmap(color_space)
def rhocolor(rho):
    return colors[int(rho*50+50)]
    

def plot_corr(axis, gene_id, m_rho, c_rho, diff, p_value):
    rhos = sorted((m_rho, c_rho))[::-1]
    rho_colors = map(rhocolor, rhos)
    axis.barh([0,1], rhos, color=rho_colors, align="edge")
    axis.set_xlim((-1,1))
    axis.set_yticks([0, 1])
    axis.set_yticklabels(["", gene_id], size=8)
    axis.set_xlabel(r"Pearson's $\rho$ Difference {:.4f} p-value {:f}".format(
        diff, p_value))


def mylog2(l):
    arr = np.array(l)
    nonzero = arr > 0
    arr[nonzero] = np.log2(arr[nonzero])
    return arr


def plot_line(axis, m_t, m_expr, m_rho, c_t, c_expr, c_rho, gene_id, monolabel):
    m_t, m_expr, c_t, c_expr = map(mylog2, (m_t, m_expr, c_t, c_expr))
    m_color, c_color = map(rhocolor, [m_rho, c_rho])
    axis.plot(m_t, m_expr, color=m_color, linestyle="-")
    axis.plot(m_t, m_expr, 'o', color=m_color, label=monolabel+" monoculture")
    axis.plot(c_t, c_expr, color=c_color, linestyle="-")
    axis.plot(c_t, c_expr, 'o', color=c_color, label="Fuso Strep Co-culture")
    axis.legend(loc="lower right")
    axis.set_ylabel("Expression (log2 RPKM)")
    axis.set_xlabel("Time (log2 minutes)")
    

def plot(m_rho, m_expr, m_t,
         c_rho, c_expr, c_t,
         diff, p_value, gene_id, monolabel, output):
    plt.figure()
    grid = GridSpec(3,3)
    line_axis = plt.subplot(grid[0:2,:])
    corr_axis = plt.subplot(grid[2,:])
    plot_corr(corr_axis, gene_id, m_rho, c_rho, diff, p_value)
    plot_line(line_axis, m_t, m_expr, m_rho,
                         c_t, c_expr, c_rho,
              gene_id, monolabel)
    plt.tight_layout()
    plt.savefig(output)
    plt.close()


def plot_correlation_dist(mono_t, mono_expr, co_t, co_expr, ids,
                          monolabel, output_dir):
    mono_rhos = pearsons_corrs(mono_expr, mono_t)
    co_rhos = pearsons_corrs(co_expr, co_t)
    diffs = (np.array(mono_rhos)-np.array(co_rhos))**2
    p_values = correlation_permutations(mono_t, mono_expr, co_t, co_expr)
    packed = zip(mono_rhos, mono_expr.T, co_rhos,
                 co_expr.T, diffs, p_values, ids)
    for m_r, m_expr, c_r, c_expr, diff, p_val, gene_id in packed:
        filename = "{}_c_{:.1f}_p{:.2f}.png".format(gene_id, diff, p_val)
        output = os.path.join(output_dir, filename)
        plot(m_r, m_expr, mono_t,
             c_r, c_expr, co_t,
             diff, p_val, gene_id, monolabel, output)

    
def main(f_basestr, s_basestr, co_basestr, sparse_thresh,
         output_dir_f, output_dir_s):
    sparse_thresh = float(sparse_thresh)
    ids, (f, s, co) = load(f_basestr, s_basestr, co_basestr)
    f_t, f_expr = f; s_t, s_expr = s; co_t, co_expr = co
    f_sfilter = is_sparse(f_expr, sparse_thresh)
    s_sfilter = is_sparse(s_expr, sparse_thresh)
    co_sfilter = is_sparse(co_expr, sparse_thresh)
    fco_sfilter = f_sfilter&co_sfilter
    os.mkdir(output_dir_f)
    plot_correlation_dist(f_t, f_expr[:,fco_sfilter],
                          co_t, co_expr[:,fco_sfilter],
                          np.array(ids)[fco_sfilter], "Fuso", output_dir_f)
    sco_sfilter = s_sfilter&co_sfilter
    os.mkdir(output_dir_s)
    plot_correlation_dist(s_t, s_expr[:,sco_sfilter],
                          co_t, co_expr[:,sco_sfilter],
                          np.array(ids)[sco_sfilter], "Strep", output_dir_s)
    
    pass

if __name__ == '__main__':
    ret = main(*sys.argv[1:])
    sys.exit(ret)
