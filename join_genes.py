from toolz import join
fields = lambda str: iter( line.strip().split('\t') for line in open(str) )
fuso = fields('fusosorted')
strep = fields('strepsorted')
joined = list(join(1, fuso, 1, strep))
output = open('joined_mapping', 'w')
for a, b in joined:
    print >> output, "\t".join([a[0], b[0], a[1]])

