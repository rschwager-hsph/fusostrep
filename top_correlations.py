import re
import sys
import operator
import contextlib
from glob import glob
from itertools import count

import numpy as np
import matplotlib.pyplot as plt
from toolz import get, first, second

figcount = count(1)

def fields(f):
    flds = iter( get([3, 6], l.strip().split('\t')) for l in f
                 if not l.startswith("Start") )
    return sorted(filter(None, flds), key=first)


def load_expression_array(name_base):
    files = map(open, glob(name_base+"*.txt"))
    with contextlib.nested(*files):
        deserialized = map(fields, files)
        first, rest = deserialized[0], deserialized[1:]
        ids, _ = zip(*first)
        expr_array = np.array([map(second, item) for item in deserialized],
                              dtype=float)
        sort_idxs = np.argsort(times(name_base))
    return ids, expr_array[sort_idxs,:]
            

def load(*name_bases):
    arrays = map(load_expression_array, name_bases)
    ids = arrays[0][0]
    return ids, map(second, arrays)

time_re = lambda s : re.search(r'(\d+)([mh]?)', s).groups()

def times(basestr):
    filenames = glob(basestr+"*.txt")
    units_m = {"m": 1, "h": 60, "": 0}
    vals, units = zip(* map(time_re, filenames))
    vals, units = map(units_m.get, units), map(int, vals)
    return map(operator.mul, vals, units)


def pearsons_corrs(expr_tab, times):
    corr_against_time = lambda row: np.corrcoef(row, times)[0,1]
    fltr = np.any(expr_tab, axis=0)
    return map(corr_against_time, expr_tab.T[fltr])


def plot(ids, rhos, output):
    color_space = np.hstack((np.linspace(1,0), np.linspace(1,0)))
    cmap = plt.get_cmap("RdYlGn")
    colors = cmap(color_space)[::-1]
    rho_colors = map(int, np.array(rhos)*50+50)
    y_pos = np.arange(len(ids))
    plt.barh(y_pos, rhos, color=colors[rho_colors], align="center")
    plt.yticks(y_pos, ids, size=8)
    plt.xlabel(r"Pearson's $\rho$")
    plt.savefig(output)

def save_rho_list(ids, rhos, output):
    with open(output, 'w') as out_f:
        for id, rho in zip(ids, rhos):
            print >> out_f, "{}\t{}".format(id, rho)


def main(basestr, plot_fname, table_fname, top_n, bot_n):
    ids, (expression,) = load(basestr)
    t = np.sort(np.array(times(basestr)))
    rhos = pearsons_corrs(expression, t)
    sort_idxs = np.argsort(rhos)
    rhos = get(list(sort_idxs), rhos)
    ids = get(list(sort_idxs), ids)
    save_rho_list(ids, rhos, table_fname)
    # argsort goes from small to large, so reverse these
    top_n, bot_n = int(bot_n), int(top_n)
    top_bot_rhos = rhos[:top_n]+rhos[-bot_n:]
    top_bot_ids = ids[:top_n]+ids[-bot_n:]
    plot(top_bot_ids, top_bot_rhos, plot_fname)


if __name__ == '__main__':
    ret = main(*sys.argv[1:])
    sys.exit(ret)
